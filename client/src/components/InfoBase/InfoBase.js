import React from 'react';
import './styles.scss'

export function InfoBase(props) {
    const items = props.items.map((item) => {
        const splitted = item.text.split(' ');
        const first = splitted[0];
        const text = item.text.substr(first.length);
        return {
            ...item,
            first,
            text
        }
    });

    return (
        <div className={'info-base-root'}>
            <div className={'info-base-inner'}>
                <span className={"info-base-title"}>
                    База знаний
                </span>

                <div className={"info-base-items"}>
                    {items.map((item, index) => {
                        return (
                            <div className={"info-base-item"} key={index}>
                                <span className={"info-base-item-first"}>{item.first}</span>
                                <span>
                                    {item.text}
                                </span>
                            </div>
                        )
                    })}
                </div>
            </div>
        </div>
    );
}