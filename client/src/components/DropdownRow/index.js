import React, {Component} from 'react';
import './styles.scss';

import {Icon} from 'atomize';

class DropdownRow extends Component {
    constructor(props) {
        super(props);

        this.state = {isOpen: false,};
    }

    clickHandler = () => {
        this.setState({isOpen: !this.state.isOpen});
    }

    render() {
        return (
            <div>
            <div className='dropdown-row'>
                <div className='dropdown-row__content'>
                    <Icon 
                        name='Alert'
                        size='20px'
                    />
                    <p className='dropdown-row__title'>Тут должен быть текст выпадающего меню</p>
                    <Icon
                        onClick={this.clickHandler} 
                        cursor="pointer"
                        name={`${this.state.isOpen ? 'DownArrow' : 'RightArrow'}`}
                        size='20px'
                    />
                </div>
                <div className={`dropdown-row__text ${this.state.isOpen ? '' : 'dropdown-row__text_hidden'}`}>
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolor in adipisci molestias voluptatibus cupiditate
                 iure maiores repudiandae! Aut fugiat quod laborum voluptates eos earum,
                 accusamus veniam laboriosam, unde facere mollitia?
                </div>
            </div>

            </div>
        )
    }
}

export default DropdownRow;
