import React, {Component} from 'react';
import './styles.scss';
import connect from 'react-redux';
import DropdownRow from '../DropdownRow';

import {Input, Icon} from 'atomize';

class TypicalAnswers extends Component {
    constructor(props) {
        super(props);

        this.state = {};
    }

    render() {
        return (
            <div className='typical-answers'>
                <h1 className='typical-answers__title'>Типовые ответы</h1>
                <div style={{marginBottom: '10px'}}>
                <Input
                    bg='gray400'
                    h='32px' 
                    w='96%' 
                    m={{x: '16px'}}
                    value={''}
                    suffix={
                        <Icon
                            name="Search"
                            size="20px"
                            cursor="pointer"
                            onClick={() => console.log("clicked")}
                            pos="absolute"
                            top="50%"
                            right="8%"
                            transform="translateY(-50%)"
                        />}
                />
                </div>
                {/*<DropdownRow />*/}
            </div>
        )
    }
}

export default TypicalAnswers;

