import React, {Component} from 'react';
import './styles.scss';

class NewRequest extends Component {
    constructor(props) {
        super(props);

        this.state = {};
    }

    troubleDescr(troubles) {
        if (!troubles) return null;

        return troubles.map(trouble => {
            return (
                <button 
                    key={`click${trouble}`}
                    onClick={() => console.log(`click ${trouble}`)} 
                    className='new-request__trouble-word'>
                        {trouble} 
                </button>
            )
        })
    }

    render() {
        const {reqNumber, date, troubles, specifyTroubles} = this.props;

        return (
            <div className='new-request'>
                <div className='new-request__content'>
                    <p className='new-request__title'>{`Новая заявка ${reqNumber} от ${date}`}</p>
                    <div>
                        <span className='new-request__trouble-title'>Описание проблемы</span>
                        <div className='new-request__trouble-descr'>
                            {this.troubleDescr(troubles)}
                        </div>
                    </div>
                    <div>
                        <span className='new-request__trouble-title'>Уточнение проблемы</span>
                        <div className='new-request__trouble-descr'>
                            {this.troubleDescr(specifyTroubles)}
                        </div>
                    </div>
                    <form>
                        <label className='new-request__form-label' for='phoneNumber'>Контактный номер телефона</label>
                        <input className='new-request__form-input new-request__form-input_long' id='phoneNumber' type='tel'></input>
                        <label className='new-request__form-label' for='address'>Адрес</label>
                        <input className='new-request__form-input new-request__form-input_long' id='address' type='text'></input>
                        <div className='new-request__wrapper'>
                            <div className='new-request__small-input-wrapper'>
                                <label className='new-request__form-label new-request__form-label_small' for='entrance'>Подъезд</label>
                                <input className='new-request__form-input new-request__form-input_small' id='entrance' type='number'></input>
                            </div>
                            <div className='new-request__small-input-wrapper'>
                                <label className='new-request__form-label new-request__form-label_small' for='floor'>Этаж</label>
                                <input className='new-request__form-input new-request__form-input_small' id='floor' type='number'></input>
                            </div>
                            <div className='new-request__small-input-wrapper'>
                                <label className='new-request__form-label new-request__form-label_small' for='flat'>Квартира</label>
                                <input className='new-request__form-input new-request__form-input_small' id='flat' type='number'></input>
                            </div>
                        </div>
                        <label className='new-request__form-label' for='contact'>Контактное лицо</label>
                        <input className='new-request__form-input new-request__form-input_long' id='contact' type='text'></input>
                        <label className='new-request__form-label' for='trouble'>Описание проблемы</label>
                        <textarea className='new-request__form-textarea' id='trouble'></textarea>
                        <div>
                            <button type='button' className='new-request__form-button' onClick={(e) => {
                                e.stopPropagation();
                                alert("Заявка успешно отправлена")
                            }}>
                                Отправить заявку
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        )
    }
}

export default NewRequest;

