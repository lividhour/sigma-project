import React from 'react';
import './styles.scss';

export function AddressCardView({address, uk, info}) {
    return (
        <div className={'address-card-root'}>
            <div className={'address-card-inner'}>
                <span className={'address-card-title'}>
                    Быстрая справка
                </span>
                <span className={'address-card-address'}>
                    Адрес: {address}
                </span>

                <div className={'address-card-info'}>
                    <div className={'address-card-info-row'}>

                    <span>
                        Управляющая компания:
                    </span>

                        <span className={'address-card-info-row-red-text'}>
                        &nbsp;{`${uk.name},`}
                    </span>

                        <span className={'address-card-info-row-green-text'}>
                        &nbsp;{`${uk.phone}`}
                    </span>
                    </div>

                    <div className={'address-card-info-row'}>
                    <span>
                       &nbsp;{`${info.title}:`}
                    </span>

                        <span className={'address-card-info-row-green-text'}>
                        &nbsp;{`${info.text}`}
                        </span>
                    </div>
                </div>
            </div>
        </div>
    );
}