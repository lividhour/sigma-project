import React, {Component} from 'react';
import './styles.scss';

class CallInfo extends Component {
    constructor(props) {
        super(props);

        this.state = {count: 0};
    }

    componentDidMount() {
        setInterval(() => this.setState({count: this.state.count + 1}), 1000);
    }

    render() {
        const {options, callType, phoneNumber} = this.props;
        const time = this.state.count < 10 ? `00:0${this.state.count}` : `00:${this.state.count}`;
        return (
            <div className='call-info'>
                <div className='call-info__content'>
                    <div className='call-info_call-type-wrapper'>
                        <span className='call-info_call-subtitle'>{`${callType} вызов`}</span>
                        <p className='call-info_number'>{phoneNumber}</p>
                    </div>
                    <div className='call-info_call-time-wrapper'>
                        <span className='call-info_call-subtitle'>Время</span>
                        <p className='call-info_number'>{time}</p>
                    </div>
                </div>
            </div>
        )
    }
}

export default CallInfo;

