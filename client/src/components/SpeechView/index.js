import React from 'react';
import {Div} from 'atomize';

const SpeechView = (props) => {
        return (
            <Div 
                minH='110px'
                d='flex'
                flexDir='column'
                border='1px solid'
                borderColor='gray500'
                m={{l: '16px', r: '16px'}}
                bg='gray400'
            >
                {props.data.map(item => (
                    <p className='speech-words' key={item}>{item}</p>
                ))}

            </Div>
        )
}

export default SpeechView;

