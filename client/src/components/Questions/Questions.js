import React from 'react';
import './styles.scss'

export function Questions({items}) {
    return (
        <div className={'questions-root'}>
            <div className={'questions-inner'}>
                <span className={"questions-title"}>
                    Уточнить у клиента
                </span>

                <div className={"questions-items"}>
                    {items.map((item, index) => {
                        return item
                            ? (
                            <div className={"questions-item"} key={index}>
                                <span>
                                    {item}?
                                </span>
                            </div>
                        )
                            : null;
                    })}
                </div>
            </div>
        </div>
    );
}