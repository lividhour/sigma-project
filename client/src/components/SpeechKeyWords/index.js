import React from 'react';
import {Div} from 'atomize';

const SpeechKeyWords = ({words, onClick}) => {
        return (
            <Div 
                d='flex'
                borderColor='gray500'
                m={{l: '16px', r: '16px'}}
                flexWrap="wrap"
            >
                {words.map(word => {
                    return (
                        <button 
                            key={`click${word}`}
                            onClick={() => onClick(word)}
                            className='speech-key-word'>
                                {word}
                        </button>
                    )
                })}
            </Div>
        )
}

export default SpeechKeyWords;

