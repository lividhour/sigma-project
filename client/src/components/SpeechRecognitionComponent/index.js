import React, {Component} from 'react';

import {Div} from 'atomize';

import SpeechView from '../SpeechView';
import SpeechKeyWords from '../SpeechKeyWords';

class SpeechRecognitionComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {};
    }

    render() {
        return (
            <Div bg='white' d='flex' flexDir='column' m={{b: '11px'}}>
                <Div>
                    <p className='speech-title'>Устное обращение пользователя</p>
                </Div>
                <SpeechView data={this.props.data}/>
                <Div>
                    <p className='speech-title'>{this.props.words.length ? 'Категория' : ''}</p>
                </Div>
                <SpeechKeyWords words={this.props.words} onClick={this.props.onClick}/>
                <Div>
                    <p className='speech-title'>{this.props.topics.length ? 'Тип заявки' : ''}</p>
                </Div>
                {this.props.topics.map(item => {
                    return (
                        <Div
                            d='flex'
                            borderColor='gray500'
                            m={{l: '16px', r: '16px'}}
                            flexWrap="wrap"
                        >
                            <button
                                key={item}
                                onClick={() => this.props.onTopicClick(item)}
                                className='speech-req-type'>
                                {item}
                            </button>
                        </Div>
                    )
                })}
            </Div>
        )
    }
}

export default SpeechRecognitionComponent;

