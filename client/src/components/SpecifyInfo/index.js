import React, {Component} from 'react';
import './styles.scss';
import connect from 'react-redux';

class SpecifyInfo extends Component {
    constructor(props) {
        super(props);

        this.state = {};
    }

    render() {
        const {options} = this.props;

        return (
            <div className='specify'>
                <div className='specify__content'>
                    <p className='specify__title'>
                        Уточнить у клиента
                    </p>
                    <p className='specify__subtitle'>
                        Необходимо точно убедиться, что заявитель имеет в виду
                    </p>
                    <div className='specify__options-container'>
                        {options.map(option => {
                            return (
                                <button 
                                    key={`btn_${option}`}
                                    className='specify__options'
                                >
                                        {option}
                                </button>
                            )
                        })}
                    </div>
                </div>
            </div>
        )
    }
}

export default SpecifyInfo;

