import React, {Component} from 'react';

import './App.scss';
import {Col, Container} from 'atomize';
import SpeechRecognitionComponent from './components/SpeechRecognitionComponent';
import {AddressCard} from './components/AdressCard';
import {Questions} from './components/Questions';
import TypicalAnswers from './components/TypicalAnswers';
import {Provider} from 'react-redux';
import CallInfo from './components/CallInfo';
import NewRequest from './components/NewRequest';
import store from './store';
import {InfoBase} from './components/InfoBase';
import {EdcApi} from './services';
import {DataApi} from './services/DataApi';

const arr = ['Новоалексеевская', '3', 'У',
    'нас', 'в', 'тамбуре', 'перегорела', 'лампочка', 'между', 'первой', 'подъездной', 'дверью', 'и', 'второй',
    'Когда', 'придет', 'мастер'];

const speechParse = [
    'Новоалексеевская 3',
    'У нас в тамбуре перегорела лампочка!'
];


class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            speech: [],
            address: '',
            uk: {},
            info: {},
            categories: [],
            topics: [],
            questions: [],
            solutions: [],
            sol: [],
        };
    }

    async componentDidMount() {
        setTimeout(async () => {
            this.state.speech.push(speechParse[0]);
            this.setState({speech: [...this.state.speech]});
            const address = await EdcApi.getAddress(speechParse[0]);

            if (address.data.data.length) {
                this.setState({address: address.data.data[0].full_name});
                const uk = await EdcApi.getOwner(address.data.data[0].id);
                if (uk) {
                    this.setState({
                        uk: {
                            name: uk.data.data[0].display_name,
                            phone: uk.data.data[0].phone
                        },
                        info: {
                            title: 'График отключения холодной воды',
                            text: '04.06.2019 - 14.06.2019'
                        }
                    })
                }
            }
        }, 1500);

        setTimeout(async () => {
            this.state.speech.push(speechParse[1]);
            this.setState({speech: [...this.state.speech]});
            const data = await DataApi.getData(speechParse[1]);

            const category = new Set(data.recommendations.map(item => item.category));
            this.setState({
                categories: Array.from(category),
                recomendations: data.recommendations,
                solutions: data.solutions,
            });
        }, 3500);
    }


    onCategoryClick = (word) => {
        const topics = [];
        this.state.recomendations.forEach(item => {
            if (item.category === word) {
                topics.push(item.topic);
            }
        });

        this.setState({topics, sol: [], questions: []})
    };

    onTopicClick = (topic) => {
        const questions = [];
        const topicIds = [];
        const sol = [];

        this.state.recomendations.forEach(item => {
            if (item.topic === topic) {
                topicIds.push(item.id);
                const q = item.questions.split('?');
                questions.push(...q);
            }
        });

        this.state.solutions.forEach((item, index) => {
            if (topicIds.find(id => id === item.topic_id)) {
                sol.push({text: item.text});
            }
        });

        this.setState({questions, sol});
    };

    render() {
        return (
            <Provider store={store}>
                <div className='App'>
                    <div className="menu">
                        <span style={{color: ' #0a1f44', fontWeight: 'bold'}}>Заявки</span>
                        <span>Консультации</span>
                        <span>Задания</span>
                        <span>Нарушения</span>
                        <span>Горячие зоны на карте города</span>
                    </div>
                    <div style={{marginTop: '10px'}}></div>
                    <Container
                        d={'flex'}
                        justify={'space-between'}
                        minW={'100vw'}
                    >
                        <Col justify='flex-start' size="3">
                            <CallInfo
                                callType={'Входящий'}
                                phoneNumber={'8(800)555-55-55'}
                                time={'15:55'}
                            />
                            <SpeechRecognitionComponent
                                data={this.state.speech}
                                words={this.state.categories}
                                onClick={this.onCategoryClick}
                                topics={this.state.topics}
                                onTopicClick={this.onTopicClick}
                            />
                            <TypicalAnswers/>
                        </Col>
                        <Col justify='flex-start' size="5">
                            {this.state.uk.name &&
                            <AddressCard
                                address={this.state.address}
                                uk={this.state.uk}
                                info={this.state.info}
                            />
                            }
                            <Questions
                                items={this.state.questions}
                            />
                            <InfoBase
                                items={this.state.sol}
                            />
                        </Col>
                        <Col size='4'>
                            <NewRequest
                                reqNumber={'№0100-0101-004210/19'}
                                troubles={['Освещение']}
                                specifyTroubles={['Перегорела лампочка']}
                                date={'26.07.2019 13:12'}
                            />
                        </Col>
                    </Container>
                </div>
            </Provider>
        );
    }
}

export default App;
