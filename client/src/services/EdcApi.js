import axios from 'axios';

const login = 'AvdeevGA';
const password = '1234';
// const loginUrl = 'http://185.173.1.70';
const addressUrl = 'http://localhost:3001/api/edc/address';
const ownerUrl = 'http://localhost:3001/api/edc/owner';

export class EdcApi {
    static async login() {
        await axios.post(`http://localhost:3001/api/auth/login`, {username: login, password});
    }

    static async getAddress(address) {
       return  (await axios.get(addressUrl, {params: {full_name: address}})).data;
    }

    static async getOwner(id) {
        return (await axios.get(ownerUrl, {params: {address_Id: id}})).data;
    }
}
