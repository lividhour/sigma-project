import axios from 'axios';
export class DataApi {
   static async getData(text) {
       return  (await axios.post('http://10.30.21.235:5000', {data: text})).data;
   }
}