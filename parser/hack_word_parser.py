# @Author Michael Pavlov
# version 1.0 2019-07-27

from bs4 import BeautifulSoup
import mysql.connector
import pymorphy2
import ast

DB_DATABASE = "hack"
DB_USER = "hacker"
DB_PASSWORD = ""
DB_HOST = ""
DB_PORT = "9999"

connection_main = mysql.connector.connect(user=DB_USER, password=DB_PASSWORD,
                                                    host=DB_HOST, port=DB_PORT,
                                                    database=DB_DATABASE)
connection_main.autocommit = True
cursor_m = connection_main.cursor()

def save_to_db(topic_id_, chapter, topic_, similar_cases_, additional_questions_,kw_):
    try:
        for result_ in cursor_m.execute("INSERT into hack_breakthrough_topics (id, chapter,topic_name,similar_cases_,additional_questions,kw) values (%s,%s,%s,%s,%s,%s)",
                                              (topic_id_,chapter,topic_, similar_cases_, additional_questions_,kw_), multi=True):
            pass
    except mysql.connector.DatabaseError as err:
        return False
    except Exception as e:
        return False
    else:
        try:
            connection_main.commit()
            print("Successfully ADDED new item")
            return True
        except Exception as e:
            print("Cant commit transaction Insert new item. " + str(e))
    return True

def save_wiki_to_db(topic_id_, topic_, text_, kw_):
    try:
        for result_ in cursor_m.execute("INSERT into hack_breakthrough_help (topic_name,topic_id,text,kw) values (%s,%s,%s,%s)",
                                              (topic_, topic_id_, text_, kw_), multi=True):
            pass
    except mysql.connector.DatabaseError as err:
        print(err)
        return False
    except Exception as e:
        print(e)
        return False
    else:
        try:
            connection_main.commit()
            print("Successfully ADDED new item")
            return True
        except Exception as e:
            print("Cant commit transaction Insert new item. " + str(e))
    return True

def parse_norm_row(row):
    # black_list = ['COMP','NUMR','ADVB','NPRO','PRED','CONJ','PRCL','INTJ','PREP','VERB','INFN']
    white_list = ['NOUN','VERB','INFN','ADJF','PRTF','PRTS','ADJS']
    pymorphy = pymorphy2.MorphAnalyzer()
    row = row.replace("/"," ")
    row = row.replace('''"'''," ").replace("("," ").replace(")"," ").replace("["," ").replace("]"," ").replace("?"," ").replace(":"," ")
    row = row.replace("'", " ")
    row = row.replace("!"," ").replace(","," ").replace("."," ").replace(";"," ").replace("«"," ").replace("»"," ")
    row = row.replace("#"," ").replace("+"," ").replace("{"," ").replace("}"," ").replace(">"," ").replace("<"," ")
    word_list = []
    for word in row.split():
        word = word.strip('\'.,!?:;-\n\r()«»*^~"$&|№%#“‘”@₽€`—=+😉😍😏😄☺😃？•…')
        form= pymorphy.parse(word)[0]
        word = form.normal_form
        if form.tag.POS in white_list:
            word_list.append(word)
    return list(set(word_list))

def html_parser_main(text_):
    try:
        topic_id_ = 1
        chapter = ""
        soup = BeautifulSoup(text_, "lxml")
        # print(soup)
        table_obj = soup.find('table')
        # print(table_obj)
        row_list = table_obj.find_all("tr", {'style' : True})
        print(len(row_list))
        for row_ in row_list:
            cells = row_.find_all("td", {'style' : True})
            try:
                # print("row:_________________")
                if len(cells) > 5:
                    # print("____full row:_________________")
                    # full row
                    topic_ = topic_parser(cells[1])
                    similar_cases_ = similar_cases_parser(cells[2])
                    additional_questions_ = additional_questions_parser(cells[3])
                    topic_kw = parse_norm_row(topic_ + " " + similar_cases_)
                    save_to_db(topic_id_, chapter, topic_, similar_cases_, additional_questions_,str(topic_kw))
                    wiki_topics = wiki_topics_parser(topic_id_,topic_,cells[5])
                    topic_id_ += 1

                elif len(cells) > 0:
                    # chapter
                    chapter = cells[0].get_text().replace("\n"," ").replace("  "," ").replace("  "," ").strip()
                    print("_chapter:_________________", chapter)
                else:
                    print("WTF?" + row_.get_text())
            except Exception as err:
                print("WTF: " + str(err))
        print(topic_id_)
    except Exception as e:
        print("WTF: " + str(e))
    return

def html_parser_santech(text_):
    try:
        topic_id_ = 240
        chapter = ""
        soup = BeautifulSoup(text_, "lxml")
        # print(soup)
        table_obj = soup.find('table')
        # print(table_obj)
        row_list = table_obj.find_all("tr", {'style' : True})
        print(len(row_list))
        for row_ in row_list:
            cells = row_.find_all("td", {'style' : True})
            try:
                # print("row:_________________")
                if len(cells) > 4:
                    # print("____full row:_________________")
                    # full row
                    topic_ = topic_parser(cells[0])
                    similar_cases_ = similar_cases_parser(cells[1])
                    additional_questions_ = additional_questions_parser(cells[2])
                    topic_kw = parse_norm_row(topic_ + " " + similar_cases_)
                    if len(topic_) > 0:
                        save_to_db(topic_id_, chapter, topic_, similar_cases_, additional_questions_,str(topic_kw))
                    wiki_topics = wiki_santech_topics_parser(topic_id_,topic_,cells[4])
                    topic_id_ += 1
                elif len(cells) > 0:
                    # chapter
                    chapter = cells[0].get_text().replace("\n"," ").replace("  "," ").replace("  "," ").strip()
                    print("_chapter:_________________", chapter)
                else:
                    print("WTF?" + row_.get_text())
            except Exception as err:
                print("WTF: " + str(err))
        print(topic_id_)
    except Exception as e:
        print("WTF: " + str(e))
    return

def topic_parser(cell_):
    result_str = cell_.get_text().replace("\n"," ").replace("  "," ").replace("  "," ").strip()
    result_str = result_str[result_str.find(".")+1:].strip()
    if result_str.rfind("=") > -1:
        result_str = result_str[:result_str.rfind("=")].strip()
    # if len(result_str) > 0:
    #     print("=======", result_str)
    return result_str

def similar_cases_parser(cell_):
    result_str = cell_.get_text().replace("\n"," ").replace("  "," ").replace("  "," ").strip()
    # print(result_str)
    return result_str

def additional_questions_parser(cell_):
    result_str = cell_.get_text().replace("\n"," ").replace("  "," ").replace("  "," ").strip()
    # print(result_str)
    return result_str

def wiki_topics_parser(topic_id_,topic_, cell_):
    data = {}

    info = cell_.find('p', {'class': 'MsoNormal'})
    # if info is not None:
    #     data["info"] = info.get_text().strip()
    paragraphs = cell_.select('p[class*="ListParagraph"]')

    if paragraphs is not None:
        for paragraph in paragraphs:
            wiki_chapter = paragraph.get_text().replace("\n"," ").replace("§"," ").strip()
            kw_ = parse_norm_row(wiki_chapter)
            save_wiki_to_db(topic_id_,topic_,wiki_chapter,str(kw_))
            # print(wiki_chapter)

    # print(data)
    # print("=======================")
    return data

def wiki_santech_topics_parser(topic_id_,topic_, cell_):
    data = {}

    info = cell_.find('p', {'class': 'MsoNormal'})
    # if info is not None:
    #     data["info"] = info.get_text().strip()
    paragraphs = cell_.select('p[class*="MsoNormal"]')

    if paragraphs is not None:
        for paragraph in paragraphs:
            wiki_chapter = paragraph.get_text().replace("\n"," ").replace("§"," ").strip()
            kw_ = parse_norm_row(wiki_chapter)
            save_wiki_to_db(topic_id_,topic_,wiki_chapter,str(kw_))
            # print(wiki_chapter)

    # print(data)
    # print("=======================")
    return data

# work with main cases
f = open("D:\\Dropbox\\hack\\main_base.htm", mode='r')
html_parser_main(f.read())
f.close()
#
# # work with water cases
f = open("D:\\Dropbox\\hack\\santech.htm", mode='r')
html_parser_santech(f.read())
f.close()





# def get_first_three(data):
#     sql_select_Query = "select id, kw from hack_breakthrough_topics"
#     cursor_m.execute(sql_select_Query)
#     records = cursor_m.fetchall()
#
#     mass_to_sort = []
#
#     for record in records:
#         mass_to_sort.append([record[0], len(list(set(ast.literal_eval(record[1])) & set(data)))])
#
#     mass_to_sort.sort(key=lambda x: x[1], reverse=True)
#     print(mass_to_sort[0], mass_to_sort[1], mass_to_sort[2])
#
#     sql_select_Query = "select * from hack_breakthrough_topics where id in (%d, %d, %d)"%(mass_to_sort[0][0], mass_to_sort[1][0], mass_to_sort[2][0])
#     cursor_m.execute(sql_select_Query)
#     records = cursor_m.fetchall()
#     return [record for record in records]
#
# print(get_first_three(parse_norm_row("сволочи плесень течь труба")))


