create table hack_breakthrough_topics
(
	id int not null,
	chapter varchar(300) null,
	topic_name varchar(300) null,
	kw text null,
	additional_questions text null,
	similar_cases_ text null,
	constraint hack_breakthrough_topics_id_uindex
		unique (id)
)
;

create table hack_breakthrough_help
(
	id int auto_increment
		primary key,
	topic_id int null,
	topic_name varchar(300) null,
	kw text null,
	text text null,
	constraint hack_breakthrough_help_id_uindex
		unique (id)
)
;

create table hack_breakthrough_faq
(
	id int auto_increment
		primary key,
	kw text null,
	chapter varchar(300) null,
	question text NOT NULL,
	answer TEXT NOT NULL,
	constraint hack_breakthrough_faq_id_uindex
		unique (id)
)
;