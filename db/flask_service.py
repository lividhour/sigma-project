from flask import Flask
from flask import json, Response, jsonify, request
import pymorphy2
from mysql import connector
import ast
from flask_cors import CORS

DB_DATABASE = ""
DB_USER = ""
DB_PASSWORD = ""
DB_HOST = ""
DB_PORT = ""

connection_main = connector.connect(user=DB_USER, password=DB_PASSWORD, host=DB_HOST, port=DB_PORT, database=DB_DATABASE)
connection_main.autocommit = True
cursor_m = connection_main.cursor()

#table//hack_breakthrough_topics
#id
#chapter
#topic_name
#kw
#additional_questions
#similar_cases

#table//hack_breakthrough_help
#id
#topic_id
#topic_name
#kw
#text

#table//hack_breakthrough_faq
#id
#kw
#chapter
#question
#answer


def get_top(data):

    sql_select_Query = "select id, kw from hack_breakthrough_topics"
    cursor_m.execute(sql_select_Query)
    records = cursor_m.fetchall()

    mass_to_sort = []

    for record in records:
        mass_to_sort.append([record[0], len(list(set(ast.literal_eval(record[1])) & set(data)))])

    mass_to_sort.sort(key=lambda x: x[1], reverse=True)

    sql_select_Query = "select id, chapter, topic_name, additional_questions from hack_breakthrough_topics where id in (%d, %d, %d)"%(mass_to_sort[0][0], mass_to_sort[1][0], mass_to_sort[2][0])
    cursor_m.execute(sql_select_Query)
    recommendations = cursor_m.fetchall()

    recomendation_list = []
    for record in recommendations:
        data = {}
        data["id"] = record[0]
        data["category"] = record[1]
        data["topic"] = record[2]
        data["questions"] = record[3]
        recomendation_list.append(data)

    sql_select_Query = "select id, topic_id, topic_name, text from hack_breakthrough_help where topic_id in (%d, %d, %d)"%(mass_to_sort[0][0], mass_to_sort[1][0], mass_to_sort[2][0])
    cursor_m.execute(sql_select_Query)
    solutions = cursor_m.fetchall()

    solution_list = []
    for record in solutions:
        data = {}

        data["id"] = record[0]
        data["topic_id"] = record[1]
        data["topic_name"] = record[2]
        data["text"] = record[3]
        solution_list.append(data)

    '''
    sql_select_Query = "select question, answer from hack_breakthrough_faq where id in (%d, %d, %d, %d, %d)" % (mass_to_sort[0][0], mass_to_sort[1][0], mass_to_sort[2][0], mass_to_sort[3][0], mass_to_sort[4][0])
    cursor_m.execute(sql_select_Query)
    faqs = cursor_m.fetchall()

    faq_list = []

    for record in faqs:
        data = {}

        data["question"] = record[0]
        data["answer"] = record[1]
        faq_list.append(data)
    '''

    return {"recommendations": recomendation_list, "solutions": solution_list}



def get_faq_first_five(data):

    sql_select_Query = "select id, kw from hack_breakthrough_faq"
    cursor_m.execute(sql_select_Query)
    records = cursor_m.fetchall()

    mass_to_sort = []

    for record in records:
        mass_to_sort.append([record[0], len(list(set(ast.literal_eval(record[1])) & set(data)))])

    mass_to_sort.sort(key=lambda x: x[1], reverse=True)

    sql_select_Query = "select question, answer from hack_breakthrough_faq where id in (%d, %d, %d, %d, %d)"%(mass_to_sort[0][0], mass_to_sort[1][0], mass_to_sort[2][0], mass_to_sort[3][0], mass_to_sort[4][0])
    cursor_m.execute(sql_select_Query)
    faqs = cursor_m.fetchall()

    faq_list = []

    for record in faqs:
        data = {}

        data["question"] = record[0]
        data["answer"] = record[1]
        faq_list.append(data)

    return {"faqs": faq_list}



def parse_norm_row(row):
    #black_list = ['COMP','NUMR','ADVB','NPRO','PRED','CONJ','PRCL','INTJ','PREP','VERB','INFN']
    white_list = ['NOUN','VERB','INFN','ADJF','PRTF','PRTS','ADJS']
    pymorphy = pymorphy2.MorphAnalyzer()
    row = row.replace("/"," ")
    row = row.replace('''"'''," ").replace("("," ").replace(")"," ").replace("["," ").replace("]"," ").replace("?"," ").replace(":"," ")
    row = row.replace("'", " ")
    row = row.replace("!"," ").replace(","," ").replace("."," ").replace(";"," ").replace("«"," ").replace("»"," ")
    row = row.replace("#"," ").replace("+"," ").replace("{"," ").replace("}"," ").replace(">"," ").replace("<"," ")
    word_list = []
    for word in row.split():
        word = word.strip('\'.,!?:;-\n\r()«»*^~"$&|№%#“‘”@₽€`—=+😉😍😏😄☺️😃？•…')
        form= pymorphy.parse(word)[0]
        word = form.normal_form
        if form.tag.POS in white_list:
            word_list.append(word)
    return list(set(word_list))


app = Flask(__name__)
app.config['JSON_AS_ASCII'] = False
CORS(app)


@app.route("/",  methods=['POST'])
def main():
    data = parse_norm_row(request.stream.read().decode("utf-8"))
    return jsonify(get_top(data))


if __name__ == "__main__":
    app.debug = True
    app.run(host='0.0.0.0')


