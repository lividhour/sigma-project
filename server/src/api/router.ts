import * as express from 'express';
import { Router } from 'express';
import { testRouter } from './test/router';
import { edcRouter } from './edc';

export const apiRouter: Router = express.Router();
apiRouter.use('/test', testRouter);
apiRouter.use('/edc', edcRouter);