import axios from 'axios';
import {
    Request,
    Response
} from 'express';

const login: string = 'AvdeevGA';
const password: string = '1234';
const url: string = 'http://185.173.1.70';
const addressUrl: string = `${url}/api/v2/dictionaries/address`;
const ownerInfoUrl: string = `${url}/api/v1/request/ownerinfo/?&address_id=1516984`;


export class EdcController {
    private static cookie: string = null;

    public static async postAuth(req: Request, resp: Response): Promise<void> {
        const res = await axios.post(`${url}/auth/login`, { username: login, password });
        this.cookie = res.headers['set-cookie'].join(' ');
        resp.json(res.data);
    }

    public static async getAddress(req: Request, resp: Response): Promise<void> {
        console.log(req.query.full_name);
        const res = await axios.get(addressUrl, {
            params: { full_name: req.query.full_name, limit: 100, is_actual: true, bti_class: 1 },
            headers: { Cookie: await EdcController.getCookie() }
        });
        resp.setHeader('Access-Control-Allow-Origin', '*');
        resp.setHeader('Access-Control-Allow-Headers', 'origin, content-type, accept');
        resp.json(res.data);
    }

    public static async getOwner(req: Request, resp: Response): Promise<void> {
        console.log(req.query);
        const res = await axios.get(ownerInfoUrl, {
            params: { full_name: req.query.address_id },
            headers: { Cookie: await EdcController.getCookie() }
        });
        resp.setHeader('Access-Control-Allow-Origin', '*');
        resp.setHeader('Access-Control-Allow-Headers', 'origin, content-type, accept');
        resp.json(res.data);
    }

    private static async getCookie() {
        if (!EdcController.cookie) {
            const res = await axios.post(`${url}/auth/login`, { username: login, password });
            EdcController.cookie = res.headers['set-cookie'].join(' ');
        }

        return EdcController.cookie;
    }
}