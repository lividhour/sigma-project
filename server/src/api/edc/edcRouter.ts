import { Router } from 'express';
import express = require('express');
import { EdcController } from './EdcController';

export const edcRouter: Router = express.Router();

edcRouter.get('/auth', EdcController.postAuth);
edcRouter.get('/address', EdcController.getAddress);
edcRouter.get('/owner', EdcController.getOwner);