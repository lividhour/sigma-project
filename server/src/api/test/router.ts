import { Router } from 'express';
import express = require('express');
import { TestController } from './TestController';

export const testRouter: Router = express.Router();

testRouter.get('/', TestController.getTest);
testRouter.get('/json', TestController.getTestJson);
testRouter.get('/error', TestController.getTestError);
