import {
    Request,
    Response
} from 'express';

export class TestController {
    public static getTest(
        req: Request,
        resp: Response
    ): void {
        resp.status(200);
        resp.send('test');
    }

    public static getTestJson(
        req: Request,
        resp: Response
    ): void {
        const testJson: Object = { json: 'json' };
        resp.status(200);
        resp.json(testJson);
    }

    public static getTestError(): void {
        throw new Error('test err');
    }
}