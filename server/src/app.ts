import * as express from 'express';
import {
    Express,
    NextFunction,
    Request,
    Response
} from 'express';
import * as bodyParser from 'body-parser';
import * as path from 'path';
import { apiRouter } from './api';
import * as morgan from 'morgan';
import * as cookieParser from 'cookie-parser';

const PORT: number = 3001;
const HOST: string = '0.0.0.0';
const STATIC_PATH: string = path.resolve('../client/build');

const app: Express = express();

// parse cookie
app.use(cookieParser());

// parse application/x-www-form-urlencode
app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());

// serve static
app.use(express.static(STATIC_PATH));

app.use(morgan('dev'));

// api router
app.use('/api', apiRouter);

app.use('/',  (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'origin, content-type, accept');
    next();
});


// error logging middleware
app.use((err: Error, req: Request, resp: Response, next: NextFunction) => {
    console.log(err);
    resp.status(500).send(err.message);
});


app.listen(PORT, HOST, () => console.log(`Server started at http://${HOST}:${PORT}`));

